import chokidar from 'chokidar'
import jdown from 'jdown'
import kebabCase from 'just-kebab-case'
import { reloadRoutes } from 'react-static/node'

chokidar.watch('content').on('all', () => reloadRoutes())

const pages = [
  'about',
  'apply',
  'cancellation',
  'contact',
  'dayCare',
  'exoticBirdCare',
  'linkExchange',
  'photos',
  'resorts',
  'resources',
  'restaurants',
  'services',
  'stores',
]

export default {
  getSiteData: () => ({
    title: 'Elite Pet Care Palm Springs',
  }),
  getRoutes: async () => {
    const content = await jdown('content')

    const routes = pages.map(el => ({
      path: `/${kebabCase(el)}`,
      component: 'src/containers/Page',
      getData: () => ({
        page: content[el],
      }),
    }))

    routes.unshift({
      path: '/',
      component: 'src/containers/Page',
      getData: () => ({
        page: content.home,
      }),
    })

    routes.push({
      is404: true,
      component: 'src/containers/404',
    })

    return routes
  },
}
