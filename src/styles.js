import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
  body {
    background: #c1dddd url('/body-bg.png') repeat-x;
    font-family: Georgia, 'Times New Roman', serif;
    padding-top: 50px;
    padding-bottom: 10px;
  }

  a { color: #61a6a6; }
  strong { color: #a2302c; }
  em { color: #8f2622; }
`
