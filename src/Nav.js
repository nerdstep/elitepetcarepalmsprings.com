import React from 'react'
import { hot } from 'react-hot-loader'
import { Toggle } from 'react-powerplug'
import { Link } from 'react-static'
import {
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  Navbar,
  NavbarToggler,
  NavItem,
  UncontrolledDropdown,
} from 'reactstrap'
import styled from 'styled-components'

const StyledNavbar = styled(Navbar)`
  background: #a7332e;
  border-color: #912622;

  .navbar-nav > .nav-item {
    background: url('/menu-tab1.png') 0 50% no-repeat;
    margin-right: 10px;
  }

  .navbar-nav .nav-link.active,
  .navbar-nav > .nav-item > a {
    color: #f3b6b1;
    text-shadow: 1px 1px 1px #7c1b18;
    font-weight: bold;
    padding-left: 30px;
  }

  .navbar-nav .dropdown.show .nav-link,
  .navbar-nav .nav-link:hover,
  .navbar-nav .nav-link:focus {
    background: url('/menu-tab2.png') 0 50% no-repeat;
    color: #ffffff;
  }
`

const Navtop = () => (
  <Toggle initial={false}>
    {({ on, toggle }) => (
      <StyledNavbar light expand="md" fixed="top">
        <NavbarToggler onClick={toggle} />
        <Collapse navbar isOpen={on}>
          <Nav navbar className="m-auto">
            <NavItem>
              <Link className="nav-link" to="/">
                Home
              </Link>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                About Us
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem>
                  <Link to="/about">Elite Mission Statement</Link>
                </DropdownItem>
                <DropdownItem>
                  <Link to="/link-exchange">Link Exchange</Link>
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem>
                  <Link to="/contact">Contact Us</Link>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Services &#038; Rates
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem>
                  <Link to="/services">Services &#038; Rates</Link>
                </DropdownItem>
                <DropdownItem>
                  <Link to="/exotic-bird-care">Exotic Bird Care</Link>
                </DropdownItem>
                <DropdownItem>
                  <Link to="/cancellation">Cancellation Policy</Link>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Visitor Information
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem>
                  <Link to="/resorts">Pet Friendly Resorts</Link>
                </DropdownItem>
                <DropdownItem>
                  <Link to="/restaurants">Pet Friendly Restaurants</Link>
                </DropdownItem>
                <DropdownItem>
                  <Link to="/stores">Pet Stores</Link>
                </DropdownItem>
                <DropdownItem>
                  <Link to="/resources">Local Pet Resources</Link>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Job Opportunities
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem>
                  <Link to="/apply">Apply to be a Sitter</Link>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <NavItem>
              <Link className="nav-link" to="/photos">
                Photo Gallery
              </Link>
            </NavItem>
          </Nav>
        </Collapse>
      </StyledNavbar>
    )}
  </Toggle>
)

export default hot(module)(Navtop)
