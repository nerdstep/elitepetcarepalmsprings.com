import React, { Fragment } from 'react'
import { hot } from 'react-hot-loader'
import { Router } from 'react-static'
import { Col, Container, Row } from 'reactstrap'
import Content from './Content'
import Nav from './Nav'
import { GlobalStyles } from './styles'

const App = () => (
  <Router>
    <Fragment>
      <GlobalStyles />
      <Nav />
      <Container>
        <Row>
          <Col>
            <img src="/header.png" className="d-block mx-auto img-fluid" />
            <Content />
          </Col>
        </Row>
      </Container>
      <Container>
        <Row>
          <Col>
            <p className="text-center">
              &copy; {new Date().getFullYear()} Elite Pet Care Palm Springs
            </p>
          </Col>
        </Row>
      </Container>
    </Fragment>
  </Router>
)

export default hot(module)(App)
