import convert from 'htmr'
import React from 'react'
import { withRouteData } from 'react-static'

export default withRouteData(({ page }) => <div>{convert(page.contents)}</div>)
