import React from 'react'
import { hot } from 'react-hot-loader'
import Routes from 'react-static-routes'
import styled from 'styled-components'

const StyledContent = styled.div`
  background: #fff;
  border: 10px solid rgba(193, 221, 221, 0.5);
  box-shadow: 0 0 12px rgba(0, 0, 0, 0.2);

  h1 {
    background: url('/h1.png') 0 50% no-repeat;
    padding-left: 38px;
    font-size: 34px;
    font-style: italic;
    font-weight: normal;
    color: #912622;
    text-align: center;
    margin-bottom: 2rem;
  }

  h2 {
    font-size: 26px;
    color: #61a6a6;
    font-weight: 600;
    margin-bottom: 1rem;
  }

  h3 {
    font-size: 18px;
    color: #929497;
    font-weight: 600;
    margin-bottom: 1rem;
  }

  ul {
    list-style-type: none;
    margin: 15px 0;
    padding: 0 0 0 25px;
  }

  ul li {
    background-image: url('/li.png');
    background-position: 0 6px;
    background-repeat: no-repeat;
    padding: 0 0 0 15px;
  }

  .box {
    border: 1px solid #a4312d;
    padding: 5px;
    float: right;
    width: 230px;
    margin-right: 7px;
    text-align: center;
  }
`

const Content = () => (
  <StyledContent className="mb-2 p-5">
    <Routes />
  </StyledContent>
)

export default hot(module)(Content)
