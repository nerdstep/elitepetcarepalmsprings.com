# Local Pet Resources

<table class="table">
  <tr>
    <th>Pet Veterinarians &amp; emergency</th>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a
            href="http://www.aspca.org/site/PageServer?pagename=pro_apcc"
            target="_blank"
            >Animal Poison Control Center</a
          ></strong
        >
      </p>
      <p>
        <a href="http://www.aspca.org/"><img src="/uploads/630855.jpg"/></a>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://www.desertanimalhospitalps.com/" target="_blank"
            >VCA Desert Animal Hospital</a
          ></strong
        >
      </p>
      <p>Full Service Veterinariany Care &amp; Hospital.</p>
    </td>
  </tr>
  <tr>
    <th>Pet training &amp; schools</th>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://www.barkbusters.com/" target="_blank"
            >Bark Busters</a
          ></strong
        >
      </p>
      <p>Home dog training with a lifetime written guarantee.</p>
    </td>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a
            href="http://www.guidedogsofthedesert.org/index2.html"
            target="_blank"
            >Guide Dogs of the Desert</a
          ></strong
        >
      </p>
      <p>
        Providing safe mobility, loving companionship and the &#8220;miracle of
        independence&#8221; through the use of guide dogs.
      </p>
    </td>
  </tr>
  <tr>
    <th>Pet breeders &amp; purebreds</th>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://www.akc.org/" target="_blank"
            >American Kennel Club</a
          ></strong
        >
      </p>
      <p>
        The largest nonprofit charitable organization to fund exclusively canine
        health research.
      </p>
    </td>
  </tr>
  <tr>
    <th>Pet rescue &amp; adoption shelters</th>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://animalsamaritans.org/" target="_blank"
            >Animal Samaritans</a
          ></strong
        >
      </p>
      <p>
        Non-profit animal welfare organization committed to eliminating the
        euthanasia of adoptable animals.
      </p>
    </td>
  </tr>
  <tr>
    <th>Pet products &amp; supplies</th>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://www.1800petmeds.com/" target="_blank"
            >1-800-Pet-Meds</a
          ></strong
        >
      </p>
      <p>America's largest pet pharmacy.</p>
    </td>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://www.bonesnscones.com/" target="_blank"
            >Bones &#8211; n &#8211; Scones</a
          ></strong
        >
      </p>
      <p>
        All-natural pet treats and foods. Promoting the full mental and physical
        health of your pet.
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://www.coldnosewarmheart.com/" target="_blank"
            >Cold Nose Warm Heart</a
          ></strong
        >
      </p>
      <p>A gift store for dog lovers.</p>
    </td>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://www.finepetidtags.com/" target="_blank"
            >Fine Pet ID Tags</a
          ></strong
        >
      </p>
      <p>
        <a href="http://www.finepetidtags.com/">
          <img src="/uploads/finepetidtags-logo2.jpg"
        /></a>
      </p>
      <p>Pet ID Tags, Dog ID Tags, Cat ID Tags for Pets</p>
    </td>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://www.homeagainpromo.com/" target="_blank"
            >Home Again</a
          ></strong
        >
      </p>
      <p>
        Comprehensive, proactive pet recovery service using microchip
        technology.
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://www.petsmart.com/" target="_blank"
            >PetSmart</a
          ></strong
        >
      </p>
      <p>Over 900 locations nationwide for all your pet needs.</p>
    </td>
  </tr>
  <tr>
    <th>Pet information &amp; education</th>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://www.pawsandhearts.org/" target="_blank"
            >Paws and Hearts</a
          ></strong
        >
      </p>
      <p>
        Enriching the lives of seniors and children through Animal Assisted
        Therapy.
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://www.petsit.com/" target="_blank"
            >Pet Sitters International</a
          ></strong
        >
      </p>
      <p>
        Pet-sitting excellence through education; the global leader in the
        pet-sitting industry.
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>
        <strong
          ><a href="http://petsitting10.com/" target="_blank"
            >Petsitting10.com</a
          ></strong
        >
      </p>
      <p>National &amp; International Pet Sitting Directory</p>
    </td>
  </tr>
</table>
