# Pet Friendly Restaurants

<table class="table">
  <tbody>
    <tr>
      <td valign="top" width="50%">
        <h2>Palm Springs</h2>
        <p>
          <a href="http://www.cheekysps.com/">Cheeky's Restaurant </a>
        </p>
        <ul>
          <li>
            <strong>Address:</strong> 622 N Palm Canyon Dr, Palm Springs, CA
          </li>
          <li><strong>Phone:</strong> 760-327-7595</li>
          <li>
            <strong>Pet Policy:</strong> Dogs are welcome at the outdoor seats.
          </li>
        </ul>
        <div><a href="http://www.nativefoods.com/">Native Foods</a></div>
        <ul>
          <li>
            <strong>Address:</strong> 1775 E. Palm Canyon Drive, Palm Springs,
            CA
          </li>
          <li><strong>Phone:</strong> 760-416-0070</li>
          <li>
            <strong>Pet Policy:</strong> Dogs are welcome at the outdoor seats.
          </li>
        </ul>
        <div>
          <a href="http://www.spencersrestaurant.com/"> Spencer’s Restaurant</a>
        </div>
        <ul>
          <li>
            <strong>Address:</strong> 701 West Baristo Road, Palm Springs, CA
          </li>
          <li><strong>Phone:</strong> 760-327-3446</li>
          <li>
            <strong>Pet Policy:</strong> Dogs are allowed at the outside tables
          </li>
        </ul>
      </td>
      <td valign="top" width="50%">
        <h2>Rancho Mirage</h2>
        <p><a href="http://www.bajafresh.com/">Baja Fresh Mexican Grill</a></p>
        <ul>
          <li>
            <strong>Address:</strong> 71-800 Highway 111, Rancho Mirage, CA
          </li>
          <li><strong>Phone:</strong> 760-674-9380</li>
          <li>
            <strong>Pet Policy:</strong> Leashed dogs are allowed at outdoor
            tables
          </li>
        </ul>
        <div>
          <a href="http://www.pierosacquapazza.com/"> Piero’s Acqua Pazza</a>
        </div>
        <ul>
          <li>
            <strong>Address:</strong> 71-800 Highway 111, Rancho Mirage, CA
          </li>
          <li><strong>Phone:</strong> 760-862-9800</li>
          <li>
            <strong>Pet Policy:</strong> Well-behaved leashed dogs are allowed
            at the outdoor tables
          </li>
        </ul>
      </td>
    </tr>
    <tr>
      <td valign="top" width="50%">
        <h2>Palm Desert</h2>
        <p><a href="http://www.backstreet-bistro.com/">Backstreet Bistro</a></p>
        <ul>
          <li><strong>Address: </strong>72820 El Paseo, Palm Desert, CA</li>
          <li><strong>Phone: </strong>760-346-6393</li>
          <li><strong>Pet Policy:</strong> Pet friendly outside patio</li>
        </ul>
        <div>
          <a href="http://www.shermansdeli.com/">
            Sherman's Deli and Bakery</a
          >
        </div>
        <ul>
          <li>
            <strong>Address:</strong> 73-161 Country Club Drive, Palm Desert, CA
          </li>
          <li><strong>Phone: </strong>760-568-1350</li>
          <li><strong>Pet Policy:</strong> Pet friendly outside patio</li>
        </ul>
        <div><a href="http://www.zpizza.com/"> Z Pizza</a></div>
        <ul>
          <li>
            <strong>Address: </strong>73607 Highway 111, Palm Desert, California
          </li>
          <li><strong>Phone:</strong> 760-568-5405</li>
          <li>
            <strong>Pet Policy:</strong> Leashed, well-mannered dogs allowed on
            outside patio
          </li>
        </ul>
      </td>
      <td valign="top" width="50%">
        <h2>La Quinta</h2>
        <p><a href="http://www.chipotle.com/">Chipotle</a></p>
        <ul>
          <li>
            <strong>Address:</strong> 79-174 H 111, Suite 101, La Quinta, CA
          </li>
          <li><strong>Phone:</strong> 760-564-3079</li>
          <li>
            <strong>Pet Policy:</strong> Leashed dogs allowed on outside patio
          </li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>
