# Services &#038; Rates

<table class="table">
  <tbody>
    <tr>
      <th>DESCRIPTION</th>
      <th>PRICE</th>
    </tr>
    <tr>
      <td>
        <strong>In-Home Pet Consultation </strong>Your friendly
        Professional Pet Sitter will come to your home to meet you
        and your pets, gather pertinent information about your
        pets and home, and get a house key to provide services.
      </td>
      <td><strong>FREE</strong></td>
    </tr>
    <tr>
      <td>
        <strong>Daily In-Home Pet Visits </strong>We'll come
        to your home for a minimum of a 30-minute visit to check
        in on your pets. When applicable, visits include play
        time, feeding, litter box cleaning, yard waste clean-up,
        and home security check.
      </td>
      <td>
        <strong>$30.00 per visit.</strong>$3.00 each additional
        pet. Inquire for details.
      </td>
    </tr>
    <tr>
      <td>
        <strong>Exotic Bird Care </strong>Let us come into your
        home to care for your exotic bird(s)! At Elite Pet Care,
        we know that exotic birds are highly intelligent and
        sensitive animals. By allowing your exotic bird(s) to stay
        in their home and maintain their routine, your departure
        will not cause them stress or anxiety. Visit includes
        feeding, cage cleaning, and/or open perch time if
        requested.
        <em
          >Please see details under Exotic Bird Care found in the
          Services and Rates tab for more information.</em
        >
      </td>
      <td>
        <strong>Varies based on service</strong>Our rates our
        tailored to your individual needs and number of exotic
        birds. Please call or email for an exact quote.
      </td>
    </tr>
    <tr>
      <td>
        <strong>In-Home Overnight Pet Sitting </strong>We'll
        spend the night in your home so your pets can maintain
        their routine in the safety and comfort of their own
        environment! Includes, when applicable, morning and
        evening private walks, feeding, litter box and/or cage
        cleaning, mail and newspaper retrieval, plant watering,
        light rotation, opening/closing blinds, and home security
        checks. See pricing information for approximate pet sitter
        arrival and departure time.
      </td>
      <td>
        <strong>$70.00 per night.</strong>Covers 7PM-7AM. $5.00
        per additional dog, $3.00 per additional cat.
      </td>
    </tr>
    <tr>
      <td>
        <strong>Private Dog Walk </strong>Minimum 30 minute visit
        where we will take your dog on a leashed walk in your
        neighborhood. We'll make sure your dog gets out to
        stretch his legs, get some fresh air and does his
        business! Feeding and basic home care can be combined at
        no charge within the 30 minute time frame.
      </td>
      <td>
        <strong>$30.00 per outing.</strong>$5.00 each additional
        dog.
      </td>
    </tr>
    <tr>
      <td>
        <strong>Doggie Evening Care </strong>Bring your pet to one
        of our private homes in the evening while you're
        enjoying a night out OR have a professional pet sitter
        come to your home to care for and be a companion to your
        pet during your absence.
      </td>
      <td>
        <strong>$30.00 1st Hour</strong>$20.00 an hour after with
        a minimum of 2 hours
        <strong>($50.00 minimum).</strong> $25.00 per hour after
        midnight.<strong> </strong>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>Private One-on-One Babysitting</strong></p>
        <p>
          Have a professional pet sitter come to your home during
          the day or evening to care for and be a companion to
          your pet during your absence.
        </p>
      </td>
      <td>
        <p><strong>$30.00 1st Hour </strong></p>
        <p>
          $20.00 an hour after with a minimum 2 hours
          <strong>($50.00 minimum)</strong>. $25.00 per hour after
          midnight. Get quote for 2 or more pets.
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <strong>Hotel Pet Sitting </strong>Let us watch your loved
        one for the day or evening in your pet-friendly hotel, bed
        and breakfast, or resort while you go out on the town.
        Great for visitors and tourists!
      </td>
      <td>
        <strong>$30.00 1st Hour</strong>$20.00 an hour after with
        a minimum 2 hours. <strong>($50.00 minimum)</strong>.
        $25.00 per hour after midnight. Get quote for multiple
        dogs and pet sitters.
      </td>
    </tr>
    <tr>
      <td>
        <strong>Home Care </strong>Let us keep your home safe,
        tidy and looking lived in while you're away! 30
        minute visit includes mail and newspaper retrieval, plant
        watering, light rotation, opening/closing blinds, and home
        security checks.
      </td>
      <td>
        <strong>$30.00 per visit</strong>No charge if combined
        within the time frame of other pet care services provided
        unless the service is at a separate location.
      </td>
    </tr>
    <tr>
      <td>
        <strong>Pet Taxi </strong>We'll transport your pet
        and necessary supplies to and from the vet, groomer,
        trainer, boarding facility, airport or other location.
      </td>
      <td>
        <strong>$25.00 each way.</strong>$1.00 per additional mile
        outside a 10-mile radius.
      </td>
    </tr>
    <tr>
      <td>
        <strong>Injection/Subcutaneous Shots/Fluids </strong>A
        trained specialist will give your pet an injection,
        subcutaneous shot or fluids per your requirements (subject
        to availability).
      </td>
      <td>
        <strong>$12 per occurrence.</strong>$30.00 if not combined
        with another in-home service.
      </td>
    </tr>
    <tr>
      <td>
        <strong>Administrating Pill &amp; Liquid Medicine </strong
        >A trained specialist will administer your specified
        medication to your pet (subject to availability).
      </td>
      <td>
        <strong>$8.00 per occurrence.</strong>$30.00 if not
        combined with another in-home service.
      </td>
    </tr>
    <tr>
      <td>
        <p>
          <strong>Holiday Services Additional Charges </strong
          >There is an additional charge for visits on the
          following holidays: Easter, Memorial Day, 4th of July,
          Labor Day, Thanksgiving Day, Christmas Eve Day,
          Christmas Day, New Year's Eve Day, New
          Year's Day
        </p>
      </td>
      <td valign="top">
        <strong>$15.00 per visit on listed holidays.</strong>
      </td>
    </tr>
  </tbody>
</table>
