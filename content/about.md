# Elite Pet Care's Mission Statement

<img class="float-left mr-5 mb-5 img-fluid" src="/uploads/pic2.png" style="height:220px;">

## Our primary goal is to provide exceptional pet care to all of your lovable pets and to completely ensure their safety, health and contentment.

### At Elite, your pets will receive loving attention and individual, customized care provided by our team of professionally trained pet sitters.

**Our goal is to positively impact the lives of pets and owners by providing outstanding, trustworthy, and professional service**
