# Doggie Day Care Exciting New Locations, Rates, and Service Offered

- **Do you worry about your dog while you are working long hours or have to be away for the day?**
- **Does your dog have separation anxiety and get lonely at home?**
- **Is your dog in need of playmates?**
- **Do you have a young puppy who needs supervision when you are not at home?**

## We have a solution for you!

<div class="embed-responsive embed-responsive-16by9 mb-4">
  <iframe
    width="300"
    height="150"
    src="//www.youtube.com/embed/wW7h9nJTR1M"
  ></iframe>
</div>

<hr />

<img class="float-right ml-5" src="/uploads/pic5.png" />

Bring your dog to a private desert home for the day so they won't be home alone! Your pet won't be lonely and will enjoy constant companionship and safety. Elite offers a cage free day care and only allows a limited number of dogs per day. Pet Taxi service is also available.

## Requirements

### Existing Clients

- Reservations appreciated but not required
- Dogs must be socialized and non-aggressive
- Dogs must be spayed and neutered
- See details below for locations of our Doggie Day Care private homes &amp; call for other locations.
- Special rates for pre-paid weekly service

### New Clients

- Request an appointment for a free consultation prior to any service
- Make appointments via the website or by contacting owner Joan Demiany
- Dogs must be socialized and non-aggressive and will undergo an evaluation during the consultation
- Dogs must be spayed and neutered and owners need to provide proof of up to date vaccinations

## Locations and Rates

### Locations

**Small, Medium and Medium/Large Dogs:**

In the private home of Elite Pet Care Owner Joan Demiany

1055 E Via Colusa<br />
Palm Springs, CA

_Phone Number:_ 760-831-8995

Day Care offered Monday-Friday<br />
No Daycare on Wednesdays<br />
9:00AM to 4:00PM

### Rates

**Daily Rate** – \$30.00 (Monday &#8211; Friday) per dog.

**After Hours &#8211; Hourly Rate** – \$15.00

_Above rates are for 9:00AM-4:00PM. Early drop off and late pick up \$15.00 per hour._
