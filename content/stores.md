# Pet Stores in the Coachella Valley

<table class="table table-bordered">
  <tbody>
    <tr>
      <td valign="top" width="50%">
        <div><strong>Palm Springs</strong></div>
        <p><a href="http://www.bonesnscones.com/shop/">Bones and Scones</a></p>
        <p>
          <a href="http://www.coldnosewarmheart.com/">Cold Nose Warm Heart</a>
        </p>
        <p><a href="http://www.petco.com/">Petco</a></p>
        <p><a href="http://www.petsmart.com/">Petsmart</a></p>
      </td>
      <td valign="top" width="50%">
        <div><strong>Cathedral City</strong></div>
        <p>Pet Luv</p>
        <div>
          <ul>
            <li><em>Address:</em> 67260 Ramon Rd, Cathedral City, CA 92234</li>
            <li><em>Phone:</em> (760) 322-1612</li>
            <li>
              <em>Description:</em> Locally owned pet store with high quality
              merchandise
            </li>
          </ul>
        </div>
      </td>
    </tr>
    <tr>
      <td valign="top" width="50%">
        <div><strong>Palm Desert</strong></div>
        <p>
          <a href="http://www.ambrosiapetdeliofca.com/">Ambrosia Pet Deli</a>
        </p>
        <p><a href="http://www.petco.com/">Petco</a></p>
        <p><a href="http://www.petsmart.com/">Petsmart</a></p>
      </td>
      <td valign="top" width="50%">
        <div><strong>La Quinta</strong></div>
        <p>Paws and Reflect</p>
        <ul>
          <li>
            <div>
              <em>Address:</em> 51230 Eisenhower Drive, La Quinta, CA 92253
            </div>
          </li>
          <li>
            <div><em>Phone:</em> 760-564-1222</div>
          </li>
          <li>
            <div>
              <em>Description:</em> Locally owned pet store with high quality
              merchandise
            </div>
          </li>
        </ul>
        <p><a href="http://www.petco.com/">Petco</a></p>
        <p><a href="http://www.petsmart.com/">Petsmart</a></p>
      </td>
    </tr>
  </tbody>
</table>
