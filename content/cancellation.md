# Cancellation Policy

<img class="float-right" src="/uploads/pic3.png">

## In Home Overnight Sitting

- **NON-REFUNDABLE** 25% deposit of total service invoice required at the time of booking. If the service is not canceled, this deposit will be applied to your total invoice.

- **Cancellation within 0-5 days of service:** Full payment charged to credit card. No refund!

- **Cancellation 6 days prior to service:** Refund minus the 25% deposit stated above.

---

## Other Services

- **Cancellation within 0-48 hours of service:** Full payment charged to credit card (no refund).

- **Cancellation within 3-7 days of service:** 25% of total service charged to credit card (75% refund).

- **Cancellation 8 days prior to service:** No payment charged to credit card (full refund).

* Reservations are made based on sitter availability. Because of this, clients returning home early must pay for **ALL** visits scheduled on the service invoice.

* No refunds will be given for an unexpected return that leads to a specific visit cancellation.
