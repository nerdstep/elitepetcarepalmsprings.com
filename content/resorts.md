# Pet Friendly Resorts Coachella Valley

<table class="table">
  <tbody>
    <tr>
      <td>
        <p><strong>Palm Springs</strong></p>
        <p><a href="http://www.delmarcoshotel.com/">Del Marcos Hotel</a></p>
        <p>
          <a href="http://www.viceroypalmsprings.com/">Viceroy Palm Springs</a>
        </p>
        <p>
          <a href="http://www.holidayinn.com/"
            >Holiday Inn Resort Palm Springs</a
          >
        </p>
        <p>
          <a href="http://www.theparkerpalmsprings.com/"
            >Le Parker Meridien Palm Springs</a
          >
        </p>
        <p><a href="http://www.villarosainn.com/">Villa Rosa Inn</a></p>
      </td>
      <td>
        <p><strong>Rancho Mirage</strong></p>
        <p>
          <a href="http://www.hiltongardeninn.com/"
            >Hilton Garden Inn Rancho Mirage</a
          >
        </p>
        <p>
          <a href="http://www.rancholaspalmas.com/"
            >Rancho Las Palmas Resort and Spa</a
          >
        </p>
        <p>
          <a href="http://www.ritzcarlton.com/"
            >The Ritz Carlton, Rancho Mirage</a
          >
        </p>
        <p>
          <a
            href="http://www.starwoodhotels.com/westin/property/overview/index.html?language=en_US&amp;propertyID=1369"
            >Westin Mission Hills Resort and Villas</a
          >
        </p>
        <p>
          <a
            href="http://www.motel6.com/reservations/motel_detail.aspx?num=1346&amp;VID=&amp;NOA=&amp;aYr=&amp;aMo=&amp;aDa=&amp;dYr=&amp;dMo=&amp;dDa=&amp;CP=&amp;TA=&amp;BTR=/AccorMaps/M6ProximityResults.aspx?searchtype=C"
            >Motel 6</a
          >
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>Palm Desert</strong></p>
        <p>
          <a href="http://www.bestwestern.com/"
            >Best Western Palm Desert Resort</a
          >
        </p>
        <p>
          <a href="http://www.comfortsuites.com/">Comfort Suites Palm Desert</a>
        </p>
        <p>
          <a href="http://www.holidayinn.com/"
            >Holiday Inn Express Palm Desert</a
          >
        </p>
        <p><a href="http://www.inn-adc.com/">Inn at Deep Canyon</a></p>
        <p>
          <a href="http://www.marriott.com/"
            >Residence Inn by the Marriott Palm Desert
          </a>
        </p>
      </td>
      <td>
        <p><strong>La Quinta</strong></p>
        <p>
          <a href="http://www.bestwestern.com/">Best Western Date Tree Hotel</a>
          <a href="http://embassysuites1.hilton.com/"></a>
        </p>
        <p>
          <a href="http://embassysuites1.hilton.com/"
            >Embassy Suites La Quinta Hotel and Spa</a
          >
        </p>
        <p>
          <a
            href="http://homewoodsuites1.hilton.com/en_US/hw/index.do?WT.srch=1"
            >Homewood Suites by the Hilton<br
          /></a>
        </p>
        <p>
          <a href="http://www.grandchampions.hyatt.com/"
            >Hyatt Grand Champions Resort</a
          >
        </p>
        <p>
          <a href="http://www.laquintaresort.com/">La Quinta Resort and Club</a>
        </p>
      </td>
    </tr>
  </tbody>
</table>
