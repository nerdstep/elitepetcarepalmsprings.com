# Exotic Bird Care

<img class="float-left mr-5" src="/uploads/picture9.jpg">

Elite Pet Care is proud to offer the specialized service of caring for exotic birds – including but not limited to many varieties of parrots both large and small.

<img class="float-right ml-5" src="/uploads/picture8.jpg">

Professional Pet Sitters Joan Demiany and Jodi Puett are Elite Pet Care’s exotic bird specialists. Together they have approximately forty years of parrot expertise. This service consists of coming to your home to care for your exotic birds in order to ensure that your departure does not disturb their comfortable routine.

We conduct a free consultation before any service to meet your exotic bird(s) and review the intricacies of their care. Our rates are tailored to your individual needs and number of exotic birds. Please call or email for an exact quote.
