# Contact Us

<img class="float-right" src="/uploads/pic6.png">

## We would love to hear from you!

Here is the contact information for Joan Demiany – local owner and General Manager of Elite Pet Care Palm Springs.

### Cell Phone

**760-831-8995**

### Email

### joandemiany@gmail.com

# Please do not hesitate to contact us with any questions or concerns.
